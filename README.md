# Nodejs postgres demo

This is a demo of interaction between nodejs and postgres. Database code is in routes/index.js. There are two alternative approaches,
first using node-postgres and second using [massivejs](https://github.com/robconery/massive-js).

The app is a basic crud api for a todo list.

## Installation and usage

Install nodejs and docker. Then:

    npm install supervisor -g
    npm install
    docker-compose up -d     # start postgres
    node models/database.js  # create tables in db
    npm start


Example crud querys:

Add an item

    curl --data "text=Testing&complete=false" http://127.0.0.1:3000/api/v1/todos2

Get all items

    curl http://127.0.0.1:3000/api/v1/todos2

Get single item

    curl http://127.0.0.1:3000/api/v1/todos2/1

Deleting

    curl -X DELETE http://127.0.0.1:3000/api/v1/todos/3

## Further info

For the massive documentation see [here](https://massive-js.readthedocs.org/en/latest/crud/)
