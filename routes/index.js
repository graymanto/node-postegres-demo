var express = require('express');
var app = express()
var router = express.Router();
var pg = require('pg');
var path = require('path');
var connectionString = require(path.join(__dirname, '../', 'config'));

var massive = require("massive")
var db = massive.connectSync({
    connectionString: connectionString
})

router.get('/api/v1/todos2', function(req, res) {
    console.log("Get with massive example 1")

    db.items.find({}, {
        order: "id asc"
    }, function(err, items) {
        if (err) {
            console.log("There was an error " + err)
            return "Sorry error"
        }
        return res.json(items)
    });
});

router.get('/api/v1/todos22', function(req, res) {
    console.log("Get with massive example 2. Streaming results for large dataset")

    var results = [];
    db.items.find({}, {
        order: "id asc",
        stream: true
    }, function(err, stream) {

        stream.on('readable', function() {
            console.log("result!!!")
            var item = stream.read();
            results.push(item)
        });

        stream.on('end', function() {
            return res.json(results)
        });
    })
});

router.get('/api/v1/todos2/:todo_id', function(req, res) {
    console.log("Get single item with massive example")

    // Grab data from the URL parameters
    var id = req.params.todo_id;
    console.log("Getting item " + id)

    db.items.find(parseInt(id), function(err, item) {
        if (err) {
            console.log("There was an error " + err)
            return "Sorry error"
        }
        return res.json(item)
    });
});

router.post('/api/v1/todos2', function(req, res) {

    var results = [];

    // Grab data from http request
    var data = {
        text: req.body.text,
        complete: false
    };

    db.items.insert(data, function(err, item) {
        db.items.find({}, {
            order: "id asc"
        }, function(err, items) {
            if (err) {
                console.log("There was an error " + err)
                return "Sorry error"
            }
            return res.json(items)
        });
    });
});

router.delete('/api/v1/todos2/:todo_id', function(req, res) {
    console.log("Delete with massive example.")
    var id = req.params.todo_id;

    db.items.destroy({
        id: parseInt(id)
    }, function(err, item) {
        if (err) {
            console.log("There was an error " + err)
            return "Sorry error"
        }

        db.items.find({}, {
            order: "id asc"
        }, function(err, items) {
            if (err) {
                console.log("There was an error " + err)
                return "Sorry error"
            }
            return res.json(items)
        });
    });
});

router.post('/api/v1/todos', function(req, res) {

    var results = [];

    // Grab data from http request
    var data = {
        text: req.body.text,
        complete: false
    };

    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function(err, client, done) {
        // Handle connection errors
        if (err) {
            done();
            console.log(err);
            return res.status(500).json({
                success: false,
                data: err
            });
        }

        // SQL Query > Insert Data
        client.query("INSERT INTO items(text, complete) values($1, $2)", [data.text, data.complete]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            return res.json(results);
        });
    });
});

router.get('/api/v1/todos', function(req, res) {
    console.log("Get with conventional node pg")

    var results = [];

    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function(err, client, done) {
        // Handle connection errors
        if (err) {
            done();
            console.log(err);
            return res.status(500).json({
                success: false,
                data: err
            });
        }

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC;");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            return res.json(results);
        });

    });

});

router.put('/api/v1/todos/:todo_id', function(req, res) {

    var results = [];

    // Grab data from the URL parameters
    var id = req.params.todo_id;

    // Grab data from http request
    var data = {
        text: req.body.text,
        complete: req.body.complete
    };

    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function(err, client, done) {
        // Handle connection errors
        if (err) {
            done();
            console.log(err);
            return res.status(500).send(json({
                success: false,
                data: err
            }));
        }

        // SQL Query > Update Data
        client.query("UPDATE items SET text=($1), complete=($2) WHERE id=($3)", [data.text, data.complete, id]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            return res.json(results);
        });
    });

});

router.delete('/api/v1/todos/:todo_id', function(req, res) {

    var results = [];

    // Grab data from the URL parameters
    var id = req.params.todo_id;


    // Get a Postgres client from the connection pool
    pg.connect(connectionString, function(err, client, done) {
        // Handle connection errors
        if (err) {
            done();
            console.log(err);
            return res.status(500).json({
                success: false,
                data: err
            });
        }

        // SQL Query > Delete Data
        client.query("DELETE FROM items WHERE id=($1)", [id]);

        // SQL Query > Select Data
        var query = client.query("SELECT * FROM items ORDER BY id ASC");

        // Stream results back one row at a time
        query.on('row', function(row) {
            results.push(row);
        });

        // After all data is returned, close connection and return results
        query.on('end', function() {
            done();
            return res.json(results);
        });
    });

});

module.exports = router;
